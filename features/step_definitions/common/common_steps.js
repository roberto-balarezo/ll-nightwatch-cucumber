// Common steps for all features.

const client = require('nightwatch-cucumber').client;
const { defineSupportCode } = require('cucumber');
const urljoin = require('url-join');

const utils = require('../../../extras/utils');
const CSS = utils.CSS;
const vrf = utils.validateResult;

let debugOutput = client.options.desiredCapabilities.debug || false;

// Functions to generate the URLs of the different pages of the site,
// indexed by name.
let pageUrls = {
    'home page': function () {
        return client.launchUrl;
    },
    'head to head': function() {
        return urljoin(client.launchUrl, 'headtohead');
    },
    'scores': function() {
        return urljoin(client.launchUrl, 'scores')
    }
};

function resolveUrl(urlSpec) {
    if (/^https?:/.test(urlSpec)) {
        // Absolute URL to go to
        return urlSpec;
    } else if (urlSpec.startsWith('/')) {
        return urljoin(client.launchUrl, urlSpec);
    }

    let urlGenerator = pageUrls[urlSpec];
    if (urlGenerator === undefined) {
        throw new Error(`The page ${urlSpec} is not registered to exist in the site!`);
    }

    return urlGenerator();
}

let menuItemSelectorMap = {
    'players': '#menu-1030-1',
    'tournaments': '#menu-1031-1',
    'scores': '#menu-1032-1',
    'stats': '#menu-1033-1',
    'rankings': '#menu-1034-1',
    'news': '#menu-1035-1',
    'photos': '#menu-1036-1',
    'videos': '#menu-1037-1',
    'health': '#menu-1038-1',
    'shop': '#menu-2002-1',
    'en español': '#menu-2013-1',
    'wta tv': '#menu-2127-1'
};

defineSupportCode(({Given, Then, When, setDefaultTimeout}) => {

    setDefaultTimeout(60000);

    Given(/^I set the browser size to (\d+)x(\d+) px/i, (width, height) => {
        width = parseInt(width);
        height = parseInt(height);
        if (isNaN(width) || isNaN(height)) {
            throw new Error('An invalid width or height was provided.');
        }
        console.log(`  Resizing window to ${width} x ${height}...`);
        client.resizeWindow(width, height);
    });

    Given(/^I navigate to ([\w\d',%:\/\.\- ]+)$/i, page => {
        let url = resolveUrl(page);
        console.log(`  Navigating to ${url}`);
        return client
            .url(url)
            .waitForDocumentLoaded(5000, 'Page loaded.');
    });

    Then(/^Check partners block is visible and has at least one logo$/i, () => {
        return client
            .getLocationInView('#block-luxbox-layouts-luxbox-partners-block')
            .elements(CSS, '#block-luxbox-layouts-luxbox-partners-block h3.block__title', vrf(function (result) {
                this.assert.equal(result.length, 1, 'Block title found.');
                this.elementIdText(result[0].ELEMENT, vrf(function (textResult) {
                    this.assert.equal(textResult.toLowerCase(), 'global partners', 'Checking block title text.');
                }));
            }))
            .elements(CSS, '#block-luxbox-layouts-luxbox-partners-block a img', vrf(function (imgResult) {
                this.assert.equal(imgResult.length > 0, true, 'There is at least one partner logo: '
                    + imgResult.length + ' found.');
            }));
    });

    Then(/^(?:Check )?I get redirected to ([\w\d',%:\/\.\- ]+)$/i, url => {
        return client.url(vrf(function (result) {
            if (/^https?:/.test(url)) {
                this.assert.equal(result, url, `Current url: ${result} is as expected: ${url}`);
                return;
            } else if (url.toLowerCase() === 'a player profile') {
                let urlPattern = new RegExp('^' + urljoin(client.launch_url, 'players', 'player', '/') + '\\d+/.*');
                this.assert.equal(urlPattern.test(result.trim()), true, 'This page has a player profile url.');
                return;
            }
            let parts = url.split('/');
            let expectedUrl = urljoin.apply(null, [client.launchUrl, ...parts.map(component => encodeURIComponent(component))]);
            this.assert.equal(result, expectedUrl, `Current url: ${result} is as expected: ${expectedUrl}`);
        }))
        .waitForDocumentLoaded(5000, 'Page loaded.');
    });

    When(/^I click on the "([\d\wáéíóúüñ,\- ]+)" menu item$/i, menuItemName => {
        let selector = menuItemSelectorMap[menuItemName.toLowerCase()];
        if (!selector) {
            throw new Error('Menu item not registered: ' + menuItemName);
        }
        let menuItemSelector = `#superfish-1 ${selector} a`;
        return client
            .click(menuItemSelector)
            .pause(500)
            .waitForDocumentLoaded(29000, 'Page loaded.');
    });

    Then(/^(?:Check )?the page title is "(.+)"$/i, title => {
        let titleSelector = 'h1#page-title';
        return client
            .assert.visible(titleSelector, 'Page title is visible.')
            .getText(titleSelector, vrf(function (textResult) {
                this.assert.equal(textResult, title, `Expected title: '${title}', '${textResult}' received.`);
            }));
    });

    Then(/^(?:Check )?the page title contains "(.+)"$/i, title => {
        let titleSelector = 'h1#page-title';
        return client
            .assert.visible(titleSelector, 'Page title is visible.')
            .getText(titleSelector, vrf(function (textResult) {
                this.assert.strictEqual(textResult.indexOf(title) > -1, true, `Title '${textResult}' contains '${title}'.`);
            }));
    });
});