// Steps to check the head to head page.

const { client } = require('nightwatch-cucumber');
const { defineSupportCode } = require('cucumber');

let utils = require('../../extras/utils');
let CSS = utils.CSS;
let vrf = utils.validateResult;

defineSupportCode(({Given, Then, When}) => {

    Then(/^The two best-ranked players are compared$/i, () => {
        let playerContainer = '.h2h-tb-playera';
        let playerWinsSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-stats .h2h-tbps-wins`;
        let playerPercentageSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-stats .h2h-tbps-pct`;
        let playerImageSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-image picture img`;
        let playerGoToBtnSelector = `#h2h-tb-container ${playerContainer} .button-gtprofile`;

        let promise = client
            .assert.visible(playerWinsSelector)
            .getText(playerWinsSelector, vrf(function(textResult) {
                this.assert.strictEqual(/^\d+$/.test(textResult.trim()), true, 'Number of wins is indeed numeric.');
            }))
            .assert.visible(playerPercentageSelector)
            .getText(playerPercentageSelector, vrf(function(textResult) {
                this.assert.strictEqual(/^\d+ %$/.test(textResult.trim()), true, 'Win percentage is in expected format.');
            }))
            .assert.visible(playerImageSelector)
            .assert.visible(playerGoToBtnSelector);

        playerContainer = '.h2h-tb-playerb';
        playerWinsSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-stats .h2h-tbps-wins`;
        playerPercentageSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-stats .h2h-tbps-pct`;
        playerImageSelector = `#h2h-tb-container ${playerContainer} .h2h-tb-p-image picture img`;
        playerGoToBtnSelector = `#h2h-tb-container ${playerContainer} .button-gtprofile`;

        promise = promise
            .assert.visible(playerWinsSelector)
            .getText(playerWinsSelector, vrf(function(textResult) {
                this.assert.strictEqual(/^\d+$/.test(textResult.trim()), true, 'Number of wins is indeed numeric.');
            }))
            .assert.visible(playerPercentageSelector)
            .getText(playerPercentageSelector, vrf(function(textResult) {
                this.assert.strictEqual(/^\d+ %$/.test(textResult.trim()), true, 'Win percentage is in expected format.');
            }))
            .assert.visible(playerImageSelector)
            .assert.visible(playerGoToBtnSelector);

        let barContainer = '.h2h-content .h2h-tb-bars';
        promise = promise
            .assert.visible(`${barContainer} .h2h-tb-vs`)
            .assert.elementPresent(`${barContainer} .h2h-tb-bar-left div`)
            .assert.elementPresent(`${barContainer} .h2h-tb-bar-right div`);

        return promise;
    });

    Then(/^The overview tab compares both players information$/i, () => {
        let tabSelector = '.tab-overview';
        let overviewTableSelector = '.content-wta-table #overview-table table';
        return client
            .getLocationInView(tabSelector)
            .click(tabSelector)
            .getText(tabSelector, vrf(function(textResult) {
                this.assert.equal(textResult.trim().toLowerCase(), 'overview', 'overview tab found.');
            }))
            .assert.cssClassPresent(tabSelector, 'active')
            .assert.visible(overviewTableSelector)
            .getLocationInView(overviewTableSelector)
            .elements(CSS, `${overviewTableSelector} tbody tr`, vrf(function(result) {
                this.assert.strictEqual(result.length, 11, 'The overview table has 11 rows.');
            }));
    });

    Then(/^The year to date tab compares the players' current year achievements$/i, () => {
        let tabSelector = '.tab-ytd';
        let yearTableSelector = '.content-wta-table #ytd-table table';

        return client
            .moveToElement(tabSelector, 0, 0)
            .click(tabSelector)
            .getText(tabSelector, vrf(function(textResult) {
                this.assert.equal(textResult.trim().toLowerCase(), 'year to date', 'year to date tab found.');
            }))
            .assert.cssClassPresent(tabSelector, 'active')
            .assert.visible(yearTableSelector)
            .moveToElement(yearTableSelector, 0, 0)
            .elements(CSS, `${yearTableSelector} tbody tr`, vrf(function(result) {
                this.assert.strictEqual(result.length, 5, 'The year to date table has 5 rows.');
            }));
    });

    Then(/^The tournament results table shows the matches both players were faced$/i, () => {
        let blockSelector = '#h2h-tournaments-block';
        let titleSelector = `${blockSelector} .h2h-block-title`;
        let singlesTableSelector = `${blockSelector} .h2h-tournaments-block-S table`;
        return client
            .assert.visible(titleSelector)
            .getText(titleSelector, vrf(function(textResult) {
                this.assert.equal(textResult.trim().toLowerCase(), 'tournament results', 'Tournament results block found.');
            }))
            .assert.visible(`${blockSelector} .button-container button.singles`)
            .assert.visible(`${blockSelector} .button-container button.doubles`)
            .assert.visible(singlesTableSelector)
            .getLocationInView(singlesTableSelector)
            .elements(CSS, `${singlesTableSelector} thead th`, vrf(function(result) {
                this.assert.strictEqual(result.length, 5, 'Tournament results singles table has 5 columns.');
            }))
            .elements(CSS, `${singlesTableSelector} tbody td`, vrf(function(result) {
                this.assert.strictEqual(result.length > 0, true, 'Tournament results singles table has at least one row.');
            }));
    });

    function getInputSelector(inputSide) {
        inputSide = inputSide.toLowerCase().trim();
        let inputSelector = '.search-player';

        if (inputSide === 'left') {
            inputSelector = `${inputSelector} .headtohead-form-playera input[name="playera"]`;
        } else if (inputSide === 'right') {
            inputSelector = `${inputSelector} .headtohead-form-playerb input[name="playerb"]`
        } else {
            throw new Error(`Cannot find input: ${inputSide}`);
        }

        return inputSelector;
    }

    When(/^I write "(.+)" on the (left|right) box$/i, (text, input) => {
        let inputSelector = getInputSelector(input);
        return client
            .click(inputSelector)
            .pause(250)
            .getValue(inputSelector, vrf(function(valueResult) {
                this.assert.equal(valueResult, '');
            }))
            .setValue(inputSelector, [text]);
    });

    When(/^I select the player name from the (left|right) box autocomplete list$/i, inputBox => {
        let autocompleteListSelector = null;
        let firstResultSelector = null;
        inputBox = inputBox.toLowerCase();

        return client
            .pause(500)
            .waitForJqueryAjaxRequest(5000)
            .elements(CSS, 'ul.ui-widget.ui-autocomplete', vrf(function(result) {
                this.assert.strictEqual(result.length >= 2, true, 'Autocomplete lists found.');

                // Take into account the new global autocomplete feature.
                if (inputBox === 'left') {
                    autocompleteListSelector = '#ui-id-1';
                } else if (inputBox === 'right') {
                    autocompleteListSelector = '#ui-id-2';
                } else {
                    throw new Error(`Input box not registered: ${inputBox}`);
                }

                firstResultSelector = `${autocompleteListSelector} li:first-child div`;
            }))
            .perform(function(client, done) {
                client
                    .assert.visible(autocompleteListSelector)
                    .getLocationInView(firstResultSelector)
                    .moveToElement(firstResultSelector, 10, 10)
                    .click(firstResultSelector);
                done();
            });
    });

    let vsPlayersSelector = '#vs-players';
    let playerAWinsSelector = '#h2h-tb-container .h2h-tb-playera .h2h-tb-p-stats .h2h-tbps-wins';
    let playerBWinsSelector = '#h2h-tb-container .h2h-tb-playerb .h2h-tb-p-stats .h2h-tbps-wins';

    When(/^I click on the compare button$/i, function() {
        let compareButtonSelector = '.headtohead-compare';
        let world = this;

        return client
            .getText(vsPlayersSelector, vrf(function(textResult) {
                world.vsPlayers = textResult.trim();
            }))
            .getText(playerAWinsSelector, vrf(function(textResult) {
                world.playerAWins = textResult.trim();
            }))
            .getText(playerBWinsSelector, vrf(function(textResult) {
                world.playerBWins = textResult.trim();
            }))
            .click(compareButtonSelector);
    });

    Then(/^The two selected players stats are compared$/i, function() {
        let world = this;
        let playerAPoints = null;
        let playerBPoints = null;
        let playerImageSelector = '#h2h-tb-container .h2h-tb-playera .h2h-tb-p-image picture img';

        return client
            .getLocationInView(playerImageSelector)
            .waitForJqueryAjaxRequest(20000)
            .pause(250)
            .getText(vsPlayersSelector, vrf(function(textResult) {
                this.assert.notEqual(world.vsPlayers, undefined);
                this.assert.notEqual(textResult.trim(), '');
                this.assert.notEqual(textResult.trim(), world.vsPlayers);
            }))
            .getText(playerAWinsSelector, vrf(function(textResult) {
                this.assert.notEqual(world.playerAWins, undefined);
                playerAPoints = textResult.trim();
            }))
            .getText(playerBWinsSelector, vrf(function(textResult) {
                this.assert.notEqual(world.playerBWins, undefined);
                playerBPoints = textResult;

                // Verify results have changed.
                this.assert.notEqual(`${playerAPoints}||${playerBPoints}`, `${world.playerAWins}||${world.playerBWins}`);
            }))
            // Keep the browser open for a while to see the results.
            .pause(5000);
    });
});