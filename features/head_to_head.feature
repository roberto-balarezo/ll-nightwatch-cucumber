# Test suite for the head to head page.

@head2head
Feature: Head To Head

    Scenario: Navigate to the Head to Head page
        Given I set the browser size to 1280x800 px
        When I navigate to head to head
        Then The page title is "Head to Head"
        And The two best-ranked players are compared
        And The overview tab compares both players information
        And The year to date tab compares the players' current year achievements
        And The tournament results table shows the matches both players were faced

    Scenario: Compare other players
        Given I write "serena williams" on the left box
        And I select the player name from the left box autocomplete list
        Then I write "maria sharapova" on the right box
        And I select the player name from the right box autocomplete list
        When I click on the compare button
        Then The two selected players stats are compared
