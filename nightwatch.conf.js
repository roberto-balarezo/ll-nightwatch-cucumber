/**
 * Nightwatch configuration for local tests.
 */

require('nightwatch-cucumber')({});

const windowWidth = 1280;
const windowHeight = 800;
const debugInfo = false;

let nightwatchConfig = {
    "output_folder": "reports",
    "custom_commands_path": "node_modules/nightwatch-custom-commands-assertions/js/commands",
    "custom_assertions_path": "node_modules/nightwatch-custom-commands-assertions/js/assertions",
    "page_objects_path": "",
    "selenium": {
        "start_process": true,
        "server_path": "./bin/selenium.jar",
        "log_path": "./reports",
        "host": "127.0.0.1",
        "port": 4444,
        "cli_args": {
            "webdriver.chrome.driver": "./bin/chromedriver"
        }
    },
    "test_settings": {
        "default": {
            "launch_url": "http://wta2-staging.cloudapp.net",
            "silent": true,
            "log_screenshot_data": false,
            "desiredCapabilities": {
                "browserName": "chrome",
                "chromeOptions": {
                    "args": [
                        "disable-web-security",
                        "test-type",
                        "incognito",
                        "disable-extensions",
                        `window-size=${windowWidth},${windowHeight}`
                    ]
                },
                "javascriptEnabled": true,
                "acceptSslCerts": true
            }
        }
    }
};

for (let key in nightwatchConfig.test_settings) {
    if (nightwatchConfig.test_settings.hasOwnProperty(key)) {
        let config = nightwatchConfig.test_settings[key];
        config['selenium_host'] = nightwatchConfig.selenium.host;
        config['selenium_port'] = nightwatchConfig.selenium.port;

        let capabilities = config.desiredCapabilities;
        if (capabilities === undefined) {
            config.desiredCapabilities = {};
            capabilities = config.desiredCapabilities;
        }
        capabilities.screenWidth = windowWidth;
        capabilities.screenHeight = windowHeight;
        capabilities.debug = debugInfo;
    }
}

module.exports = nightwatchConfig;
