/*
 * Utility functions for the tests.
 */

const CSS = 'css selector';

/**
 * Wrapper callback that validates the result.status is zero to continue executing the code. if it is not, print
 * the result to standard error, so we can know the reason.
 * Also, as a convenience, the argument passed to the function is directly the value returned by the command
 * (result.value).
 * @param errorCallback A callback to call in case the result status is nonzero, and before the exception is thrown.
 */
function validateResult(originalCallback, errorCallback) {
    return function(result) {
        if (result.status !== 0) {
            console.error(result);
            if (errorCallback !== undefined && typeof errorCallback === 'function') {
                errorCallback();
            }
            throw new Error('Error executing command!');
        }
        originalCallback.call(this, result.value);
    };
}

function textNotEmptyChecker(item) {
    return function(textResult) {
        if (textResult.status !== 0) {
            console.error(textResult);
            throw new Error('Could not get text of element.');
        }
        this.assert.notEqual(textResult.value.trim(), '', `Checking ${item} is not empty.`);
    };
}

module.exports = {
    CSS,
    validateResult,
    txtNotEmptyChecker: textNotEmptyChecker
};
