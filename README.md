# Nightwatch test scripts for WTA site

## Requirements
For this project you need to have Java (preferably JDK8+) installed.



## Setup
The project includes an installer for Selenium and Chromedriver. These commands will download the latest selenium server and the chrome driver binary:

```
$ npm install
$ npm run setup:chrome
```

## Running the tests
This suite is run against the staging site by default. If you want to change the URL of the site the tests are run against, change the `launch_url` of the default
environment in `nightwatch.conf.js`:
```
"test_settings": {
        "default": {
            "launch_url": "http://wta2-staging.cloudapp.net",
            "silent": true,
            [...]
```
Tests are written in both Gherkin format and Javascript using Nightwatch commands.
The `features` folder contains the files in Gherkin format while the folder `features/step_definitions` contains the implementation of every Gherkin step in Javascript code.
The command

```
$ npm run test
```
runs the test suite of the site against Google Chrome.

## Running a specific test suite
You can run a specific feature using the tag declared on it. For example, the home page feature has the tag `@home` which allows you to run only this suite with this command:
```
$ node_modules/.bin/nightwatch -e default --tag home
```
Multiple tags can be specified separating them with commas:
```
$ node_modules/.bin/nightwatch -e default --tag home,head2head
```

## Excluding test suites
You can exclude some test suites in a similar fashion as you can run only certain suites. Exclude features using their tags like this:
```
$ node_modules/.bin/nightwatch -e default --skiptags home,head2head
```

## Reports

The test results are written to the console. Nightwatch also produces reports in files in the `reports` folder.
